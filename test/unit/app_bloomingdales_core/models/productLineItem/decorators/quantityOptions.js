'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var quantityOptions = proxyquire('../../../../../../cartridges/app_atg_core/cartridge/models/productLineItem/decorators/quantityOptions', {
    'dw/catalog/ProductInventoryMgr': require('../../../../../mocks/dw/catalog/ProductInventoryMgr'),
    '*/cartridge/config/preferences': {
        maxOrderQty: 10
    }
});

describe('product line item quantity options decorator', function () {
    describe('When no inventory list provided', function () {
        var productLineItemMock = {
            product: {
                availabilityModel: {
                    inventoryRecord: {
                        ATS: {
                            value: 5
                        }
                    }
                },
                minOrderQuantity: {
                    value: 1
                },
                custom: {
                    maxOrderQuantity: 6
                }
            },
            custom : {
                omsTotalStockOnHand: null
            }
        };

        it('should create quantityOptions property for passed in object', function () {
            var object = {};
            quantityOptions(object, productLineItemMock, 1);

            assert.equal(object.quantityOptions.minOrderQuantity, 1);
            assert.equal(object.quantityOptions.maxOrderQuantity, 5);
        });

        it('should handle no minOrderQuantity on the product', function () {
            var object = {};
            productLineItemMock.product.minOrderQuantity.value = null;
            quantityOptions(object, productLineItemMock, 1);

            assert.equal(object.quantityOptions.minOrderQuantity, 1);
            assert.equal(object.quantityOptions.maxOrderQuantity, 5);
        });

        it('should handle perpetual inventory on the product', function () {
            var object = {};
            productLineItemMock.product.availabilityModel.inventoryRecord.perpetual = true;
            quantityOptions(object, productLineItemMock, 1);

            assert.equal(object.quantityOptions.minOrderQuantity, 1);
            assert.equal(object.quantityOptions.maxOrderQuantity, 6);
        });

        it('should handle maxOrderQuantity on the product', function () {
            var object = {};
            productLineItemMock.product.custom.maxOrderQuantity = 2;
            quantityOptions(object, productLineItemMock, 1);

            assert.equal(object.quantityOptions.minOrderQuantity, 1);
            assert.equal(object.quantityOptions.maxOrderQuantity, 2);
        });
    });

    describe('When inventory list provided', function () {
        it('should return inventory of the specified productInventoryListID', function () {
            var productLineItemMock = {
                product: {
                    availabilityModel: {
                        inventoryRecord: {
                            ATS: {
                                value: 5
                            }
                        }
                    },
                    minOrderQuantity: {
                        value: 2
                    },
                    ID: '000002',
                    custom: {
                        stockThreshold: 0
                    }
                },
                productInventoryListID: 'inventoryListId0001',
                custom: {
                    omsTotalStockOnHand: null
                }
            };

            var object = {};
            quantityOptions(object, productLineItemMock, 1);

            assert.equal(object.quantityOptions.minOrderQuantity, 2);
            assert.equal(object.quantityOptions.maxOrderQuantity, 3);
        });
    });
});
