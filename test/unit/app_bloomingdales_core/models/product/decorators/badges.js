'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var Collection = require('../../../../../mocks/dw.util.Collection');


var productMock = {
    custom: {
        new: true,
        sale: true,
        badges: []
    }
};

var productMock2 = {
    custom: {
        new: true,
        sale: false,
        badges: [
            'limited',
            'super limited'
        ]
    }
};

var promotions = new Collection();

describe('product custom decorator', function () {
    var badges = proxyquire('../../../../../../cartridges/app_atg_core/cartridge/models/product/decorators/badges', {
        'dw/web/Resource': {
            msg: function () {
                return 'some string';
            }
        },
        'dw/system/Site': {
            current: {
                getCustomPreferenceValue: function () {
                    return 'sale,new,shipping,manual1,manual2';
                }
            }
        }
    });

    it('should create a sorted badges propertie on the passed in object based on apiProduct', function () {
        var object = {};
        badges(object, productMock, promotions);

        assert.equal(object.badges.length, 2);
        assert.equal(object.badges[0].value, 'sale');
        assert.equal(object.badges[1].value, 'new');
    });

    it('should create a sorted and sliced badges propertie on the passed in object based on apiProduct', function () {
        var object = {};
        badges(object, productMock2, promotions);

        assert.equal(object.badges.length, 2);
        assert.equal(object.badges[0].value, 'new');
        assert.equal(object.badges[1].displayValue, 'limited');
    });

    it('should create a sorted and sliced badges shipping value above manual badges', function () {
        var object = {};
        promotions = new Collection([{
            tags: ['showAsBadge'],
            name: 'free shipping'
        }]);
        badges(object, productMock2, promotions);

        assert.equal(object.badges.length, 2);
        assert.equal(object.badges[0].value, 'new');
        assert.equal(object.badges[1].displayValue, 'free shipping');
    });

});
