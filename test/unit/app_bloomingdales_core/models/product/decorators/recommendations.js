'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var productMock = { id: '1000551122' };

describe('product quantity decorator', function () {
    var recommendations = proxyquire('../../../../../../cartridges/app_atg_core/cartridge/models/product/decorators/recommendations', {
        '*/cartridge/scripts/helpers/productHelpers': {
            getRecommendations: function(product) {
                return [
                    {
                        recommendationType: 6,
                        title: 'Some title',
                        products: [
                            {
                                ID: '00010'
                            },
                            {
                                ID: '00022'
                            }
                        ]
                    },
                    {
                        recommendationType: 6,
                        title: 'Some title',
                        products: [
                            {
                                ID: '00010'
                            },
                            {
                                ID: '00022'
                            }
                        ]
                    },
                    {
                        recommendationType: 6,
                        title: 'Some title',
                        products: [
                            {
                                ID: '00010'
                            },
                            {
                                ID: '00022'
                            }
                        ]
                    }
                ];
            }
        }
    });

    it('should create a property on the passed in object called recommendations', function () {
        var object = {};
        recommendations(object, productMock);

        assert.equal(object.recommendations.length, 3);
        assert.equal(object.recommendations[0].products[0].ID, '00010');
        assert.equal(object.recommendations[0].products[1].ID, '00022');
    });
});
