'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var productMock = {
    custom: {
        furniture: false,
        returnable: true
    },
    getPrimaryCategory: function() {
        return {
            custom: {
                isBeauty: false
            }
        }
    }
};

var productMock2 = {
    custom: {
        furniture: true,
        returnable: false
    },
    getPrimaryCategory: function() {
        return {
            custom: {
                isBeauty: false
            }
        }
    }
};

describe('product custom decorator', function () {
    var quantity = proxyquire('../../../../../../cartridges/app_atg_core/cartridge/models/product/decorators/custom', {});

    it('should create a properties on the passed in object based on apiProduct', function () {
        var object = {};
        quantity(object, productMock, 1);

        assert.equal(object.furniture, false);
        assert.equal(object.returnable, true);
    });

    it('should create a properties on the passed in object based on apiProduct', function () {
        var object = {};
        quantity(object, productMock2);

        assert.equal(object.furniture, true);
        assert.equal(object.returnable, false);
    });

});
