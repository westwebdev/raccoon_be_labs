'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var productMock = {
    minOrderQuantity: {
        value: 1
    },
    custom: {
        maxOrderQuantity: 5
    }
};

describe('product quantity decorator', function () {
    var quantity = proxyquire('../../../../../../cartridges/app_atg_core/cartridge/models/product/decorators/quantity', {
        '*/cartridge/config/preferences': {
            maxOrderQty: 10
        }
    });

    it('should create a property on the passed in object called selectedQuantity', function () {
        var object = {
            availability: {
                availableQuantity: 3
            }
        };
        quantity(object, productMock, 1);

        assert.equal(object.selectedQuantity, 1);
    });

    it('should handle null quantity', function () {
        var object = {};
        quantity(object, productMock);

        assert.equal(object.selectedQuantity, 1);
    });

    it('should handle empty product', function () {
        var object = {};
        quantity(object, {}, 1);

        assert.equal(object.selectedQuantity, 1);
    });

    it('should handle empty product and no quantity', function () {
        var object = {};
        quantity(object, {});

        assert.equal(object.selectedQuantity, 1);
    });

    it('should create a property on the passed in object called minOrderQuantity', function () {
        var object = {};
        quantity(object, productMock, 1);

        assert.equal(object.minOrderQuantity, 1);
    });

    it('should create a property on the passed in object called maxOrderQuantity', function () {
        var object = {
            availability: {
                availableQuantity: 10
            }
        };
        quantity(object, productMock, 1);

        assert.equal(object.maxOrderQuantity, 5);
    });
});
