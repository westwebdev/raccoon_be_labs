'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');


describe('Product Suggestions model', function () {
    var nextProductStub = sinon.stub();
    var nextPhraseStub = sinon.stub();
    var urlStub = sinon.stub();
    var requireStub = {
        'dw/web/URLUtils': { url: urlStub },
        'dw/system/Site': {
            current: {
                getCustomPreferenceValue: function() {
                    return true;
                }
            }
        },
        '*/cartridge/config/config': {},
        '*/cartridge/models/product/productImages': function (obj, args) {
            return {
                [args.types[0]]: [
                    {
                        url: 'image url'
                    }
                ]
            }
        },
        'dw/catalog/ProductSearchModel': function () {
            return {
                setSearchPhrase: function() {},
                search: function() {},
                count: 2
            }
        },
        '*/cartridge/scripts/factories/price': {
            getPrice: function() {
                return 'price';
            }
        },
        '*/cartridge/config/preferences': function() {
            return {
                maxOrderQty: 6,
                defaultPageSize: 24,
                storefrontProductPriceThresholdEnabled: false,
                storefrontProductPriceThreshold:  0
            }
        }
    };
    var ProductSuggestions = proxyquire(process.cwd() + '/cartridges/app_atg_core/cartridge/models/search/suggestions/product.js', requireStub);
    var variationModel = {
        defaultVariant: {
            getImage: function () {
                return {
                    URL: {
                        toString: function () { return 'image url'; }
                    }
                };
            }
        }
    };
    var product1 = {
        productSearchHit: {
            product: {
                name: 'Content 1',
                ID: 1,
                master: true,
                variationModel: variationModel,
                variants: [],
                custom: {
                    visibleOnlyInHiddenCategory: false,
                    notifiable: true
                },
                priceModel: {
                    price: {
                        available: true,
                        value: 200
                    }
                },
                availabilityModel: {
                    inStock: true
                }
            }
        }
    };
    var product2 = {
        productSearchHit: {
            product: {
                name: 'Content 2',
                ID: 2,
                master: true,
                variationModel: variationModel,
                variants: [],
                custom: {
                    visibleOnlyInHiddenCategory: false,
                    notifiable:true
                },
                priceModel: {
                    price: {
                        available: true,
                        value: 200
                    }
                },
                availabilityModel: {
                    inStock: true
                }
            }
        }
    };
    var product3 = {
        productSearchHit: {
            product: {
                name: 'Content 3',
                ID: 3,
                master: true,
                variationModel: variationModel,
                variants: [],
                custom: {
                    visibleOnlyInHiddenCategory: false,
                    notifiable: true
                },
                priceModel: {
                    price: {
                        available: true,
                        value: 200
                    }
                },
                availabilityModel: {
                    inStock: true
                }
            }
        }
    };
    var phrase1 = {
        exactMatch: true,
        phrase: 'phrase 1'
    };
    var phrase2 = {
        exactMatch: true,
        phrase: 'phrase 2'
    };
    var phrase3 = {
        exactMatch: true,
        phrase: 'phrase 3'
    };

    beforeEach(function () {
        urlStub.onCall(0).returns('url1');
        urlStub.onCall(1).returns('url2');
        urlStub.onCall(2).returns('url3');

        nextProductStub.onCall(0).returns(product1);
        nextProductStub.onCall(1).returns(product2);
        nextProductStub.onCall(2).returns(product3);

        nextPhraseStub.onCall(0).returns(phrase1);
        nextPhraseStub.onCall(1).returns(phrase2);
        nextPhraseStub.onCall(2).returns(phrase3);
    });

    afterEach(function () {
        urlStub.reset()
        nextProductStub.reset();
        nextPhraseStub.reset();
    });

    var suggestions = {
        productSuggestions: {
            searchPhraseSuggestions: {
                suggestedPhrases: {
                    hasNext: function () { return true; },
                    next: nextPhraseStub
                }
            },
            suggestedProducts: {
                hasNext: function () { return true; },
                next: nextProductStub
            },
            hasSuggestions: function () { return true; }
        }
    };

    it('should product a ProductSuggestions instance', function () {
        var productSuggestions = new ProductSuggestions(suggestions, 3);
        assert.deepEqual(productSuggestions, {
            available: true,
            phrases: [{
                exactMatch: true,
                value: 'phrase 1',
                number: 2
            }, {
                exactMatch: true,
                value: 'phrase 2',
                number: 2
            }, {
                exactMatch: true,
                value: 'phrase 3',
                number: 2
            }],
            products: [{
                id: 1,
                imageUrl: 'image url',
                name: 'Content 1',
                price:'price',
                notifiable:false,
                url: 'url1',
                variants: []
            }, {
                id: 2,
                imageUrl: 'image url',
                name: 'Content 2',
                price:'price',
                notifiable:false,
                url: 'url2',
                variants: []
            }, {
                id: 3,
                imageUrl: 'image url',
                name: 'Content 3',
                price:'price',
                notifiable:false,
                url: 'url3',
                variants: []
            }]
        });
    });

    it('should not return suggestions', function () {
        product1.productSearchHit.product.custom.visibleOnlyInHiddenCategory = true;
        product2.productSearchHit.product.custom.visibleOnlyInHiddenCategory = true;
        product3.productSearchHit.product.custom.visibleOnlyInHiddenCategory = true;
        var productSuggestions = new ProductSuggestions(suggestions, 0);
        assert.equal(productSuggestions.products.length, 0);
    });
});
