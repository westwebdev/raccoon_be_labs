'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var allowedLocales;

describe('locale', function () {
    var LocaleModel = proxyquire('../../../../cartridges/app_atg_core/cartridge/models/locale', {
        '*/cartridge/config/countries': [{
            id: 'en_AE',
            currencyCode: 'AED',
            siteId: 'BloomingDales_AE',
            default: true
        }, {
            id: 'ar_AE',
            currencyCode: 'AED',
            siteId: 'BloomingDales_AE'
        }, {
            id: 'ar_KW',
            currencyCode: 'KWD',
            siteId: 'BloomingDales_KW',
            default: true
        }, {
            id: 'en_KW',
            currencyCode: 'KWD',
            siteId: 'BloomingDales_KW'
        }],
        '*/cartridge/scripts/helpers/countryHelper': {
            getCountryList: function () {
                return [
                    { id: "en_AE", currencyCode: "AED", siteId: "BloomingDales_AE", default: true },
                    { id: "ar_AE", currencyCode: "AED", siteId: "BloomingDales_AE" },
                    { id: "ar_KW", currencyCode: "KWD", siteId: "BloomingDales_KW", default: true },
                    { id: "en_KW", currencyCode: "KWD", siteId: "BloomingDales_KW" }
                ];
            }
        },
        'dw/util/Locale': {
            getLocale: function (localeID) {
                var returnValue;
                switch (localeID) {
                    case 'en_AE':
                        returnValue = {
                            country: 'AE',
                            displayCountry: 'United Arab Emirates',
                            currencyCode: 'AED',
                            displayName: 'English (AE)',
                            language: 'en',
                            displayLanguage: 'English'
                        };
                        break;
                    case 'ar_AE':
                        returnValue = {
                            country: 'AE',
                            displayCountry: 'الإمارات العربية المتحدة',
                            currencyCode: 'AED',
                            displayName: 'العربية (الإمارات العربية المتحدة)',
                            language: 'ar',
                            displayLanguage: 'العربية'
                        };
                        break;
                    case 'ar_KW':
                        returnValue = {
                            country: 'KW',
                            displayCountry: 'الكويت',
                            currencyCode: 'KWD',
                            displayName: 'العربية (الكويت)',
                            language: 'ar',
                            displayLanguage: 'العربية'
                        };
                        break;
                    case 'en_KW':
                        returnValue = {
                            country: 'KW',
                            displayCountry: 'Kuwait',
                            currencyCode: 'KWD',
                            displayName: 'English (Kuwait)',
                            language: 'en',
                            displayLanguage: 'English'

                        };
                        break;
                    default:
                        returnValue = null;
                }
                return returnValue;
            },
            ID: 'LocaleID'
        },
        'dw/web/Resource': {
            msg: function (param) {
                return param;
            }
        }
    });

    before(function () {
        allowedLocales = ['en_AE', 'ar_AE', 'ar_KW', 'en_KW'];
    });
    it('should expected locales for en_AE', function () {
        var currentLocale = {
            ID: 'en_AE',
            displayCountry: 'United Arab Emirates',
            country: 'AE',
            displayName: 'English (United Arab Emirates)',
            language: 'en',
            displayLanguage: 'English'
        };
        var siteId = 'BloomingDales_AE';
        var localeModel = new LocaleModel(currentLocale, allowedLocales, siteId);
        assert.deepEqual(localeModel, {
            'locale': {
                'countryCode': 'AE',
                'name': 'United Arab Emirates',
                'localLinks': {
                    'languages': [{
                        'localID': 'ar_AE',
                        'language': 'ar',
                        'displayName': 'العربية'
                    }],
                    'countries': [{
                        'localID': 'en_KW',
                        'country': 'KW',
                        'displayName': 'KW',
                        'longDisplayName': 'Kuwait'
                    }]
                },
                'currencyCode': 'AED',
                'displayName': 'AE',
                'language': 'en',
                'displayLanguage': 'English'
            }
        });
    });
    it('should return proper ar_KW info', function () {
        var currentLocale = {
            ID: 'ar_KW',
            displayCountry: 'الكويت',
            country: 'KW',
            displayName: 'العربية (الكويت)',
            language: 'ar',
            displayLanguage: 'العربية'
        };
        var siteId = 'BloomingDales_KW';
        var localeModel = new LocaleModel(currentLocale, allowedLocales, siteId);
        assert.deepEqual(localeModel, {
            'locale': {
                'countryCode': 'KW',
                'name': 'الكويت',
                'localLinks': {
                    'languages': [{
                        'localID': 'en_KW',
                        'language': 'en',
                        'displayName': 'English'
                    }],
                    'countries': [{
                        'localID': 'ar_AE',
                        'country': 'AE',
                        'displayName': 'AE',
                        'longDisplayName': 'الإمارات العربية المتحدة'
                    }]
                },
                'currencyCode': 'KWD',
                'displayName': 'KW',
                'language': 'ar',
                'displayLanguage': 'العربية'
            }
        });
    });
    it('should return a currentCountry if currentLocale is not set', function () {
        var currentLocale = {
            ID: '',
            displayCountry: '',
            country: '',
            displayName: '',
            language: '',
            displayLanguage: ''
        };
        var siteId = 'BloomingDales_AE';
        var localeModel = new LocaleModel(currentLocale, allowedLocales, siteId);
        assert.deepEqual(localeModel, {
            'locale': {
                'countryCode': '',
                'currencyCode': 'AED',
                'name': '',
                'displayName': '',
                'language': '',
                'displayLanguage': '',
                'localLinks': {
                    'countries': [],
                    'languages': []
                }
            }
        });
    });
});
