'use strict';

var assert = require('chai').assert;
var sinon = require('sinon');
var mockSuperModule = require('../../../../mockModuleSuperModule.js');
var baseCartHelpersMock = require('../../../../mocks/helpers/cartHelpers.js');
var cartHelpersCommon = require('../../../../mocks/helpers/cartHelpers_common.js');
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var collections = require('../../../../mocks/util/collections');
var storeMgr = require('../../../../mocks/dw/catalog/StoreMgr');
var ShippingMgr = require('../../../../mocks/dw/order/ShippingMgr');
var arrayHelper = require('../../../../mocks/util/array');
var cartHelpers;

var mockOptions = [{
    optionId: 'option 1',
    selectedValueId: '123'
}];

var createApiBasket = function () {
    var currentBasket = {
        defaultShipment: {},
        createProductLineItem: function () {
            return {
                setQuantityValue: function () {
                    return;
                },
                product: {
                    custom: {
                        availableForInStorePickup: false
                    }
                }
            };
        },
        getAllProductLineItems: function (pid) {
            return {
                currentIndex: 0,
                iterator: function () {
                    var data = [{
                        productId: pid,
                        custom: {
                            omsTotalStockOnHand: null
                        }
                    }];
                    return {
                        hasNext: function () {
                            return this.currentIndex < data.length;
                        },
                        next: function () {
                            return data[this.currentIndex++];
                        }
                    }
                },
            };
        }
    };
    return currentBasket;
};

describe('cartHelpers core cartridge', function () {
    before(function () {
        mockSuperModule.create(baseCartHelpersMock);

        cartHelpers = proxyquire('../../../../../cartridges/app_atg_core/cartridge/scripts/cart/cartHelpers', {
            'dw/catalog/StoreMgr': storeMgr,
            'dw/catalog/ProductMgr': {
                getProduct: function (productId) {
                    var ATSValue;
                    var maxOrderQuantityValue;
                    var stockThreshold;
                    if (productId === 'checkMaxQtyProduct') {
                        ATSValue = 100;
                        maxOrderQuantityValue = 3;
                        stockThreshold = 0;
                    } else if (productId === 'checkThresholdQtyProduct') {
                        ATSValue = 8;
                        stockThreshold = 6;
                        maxOrderQuantityValue = 8;
                    } else {
                        ATSValue = 8;
                        maxOrderQuantityValue = 100;
                        stockThreshold = 0;
                    }
                    return {
                        productId: productId,
                        optionModel: {
                            getOption: function () {},
                            getOptionValue: function () {},
                            setSelectedOptionValue: function () {}
                        },
                        availabilityModel: {
                            inventoryRecord: {
                                ATS: {
                                    value: ATSValue
                                }
                            },
                            isOrderable: function() { return true; }
                        },
                        custom: {
                            maxOrderQuantity: maxOrderQuantityValue,
                            stockThreshold: stockThreshold
                        }
                    };
                }
            },
            'dw/order/ShippingMgr': ShippingMgr,
            'dw/util/UUIDUtils': {
                createUUID: cartHelpersCommon.createUUID
            },
            'dw/web/URLUtils': {
                https: cartHelpersCommon.https
            },
            'dw/system/Transaction': {
                wrap: cartHelpersCommon.wrap
            },
            'dw/web/Resource': {
                msg: cartHelpersCommon.msg,
                msgf: cartHelpersCommon.msgf
            },
            'dw/customer/ProductListMgr': {
                getProductList: cartHelpersCommon.getProductList
            },
            '*/cartridge/scripts/helpers/productHelpers': {
                getOptions: cartHelpersCommon.getOptions,
                getCurrentOptionModel: cartHelpersCommon.getCurrentOptionModel
            },
            '*/cartridge/scripts/helpers/instorePickupStoreHelpers': {
                setStoreInProductLineItem: cartHelpersCommon.setStoreInProductLineItem
            },
            '*/cartridge/scripts/overlayHelper': {
                isPluginEnabled: function (sitePreference) {
                    return sitePreference === 'InStorePickup';
                },
                enabledPlugins: cartHelpersCommon.enabledPlugins,
                appendPluginPreferences: cartHelpersCommon.appendPluginPreferences
            },
            '*/cartridge/scripts/util/array': arrayHelper,
            '*/cartridge/scripts/util/collections': collections,
            '*/cartridge/scripts/checkout/checkoutHelpers': {
                copyShippingAddressToShipment: cartHelpersCommon.copyShippingAddressToShipment
            },
            'dw/catalog/ProductInventoryMgr': {
                getInventoryList: function() { return null; }
            },
            '*/cartridge/scripts/helpers/stockHelpers': {
                getOmsTotalStockOnHandValue: function () {
                    return null;
                },
                getStoreInventoryRecord: function () {
                    return null;
                }
            },
            'dw/util/StringUtils': {
                formatMoney: function () {
                    return 'formatMoney';
                }
            },
            'dw/system/Site': {
                current: {
                    getCustomPreferenceValue: function () {
                        return null;
                    }
                }
            },
            '*/cartridge/scripts/checkout/shippingHelpers': {
                isStorePickupShipment: function () {
                    return false;
                },
                isEmailShipment: function () {
                    return false;
                }
            }
        });
    });

    after(function () {
        mockSuperModule.remove();
    });

    describe('cartHelpers core - addProductToCart', function () {

        it('should add a product to the cart', function () {
            var currentBasket = createApiBasket();
            var spy = sinon.spy(currentBasket, 'createProductLineItem');
            spy.withArgs(1);

            cartHelpers.addProductToCart(currentBasket, 'someProductID', 1, [], mockOptions);
            assert.isTrue(spy.calledOnce);
            currentBasket.createProductLineItem.restore();
        });

        it('should not add a product to the cart because of ATS', function () {
            var currentBasket = createApiBasket();

            var result = cartHelpers.addProductToCart(currentBasket, 'someProductID', 9, [], mockOptions);
            assert.isTrue(result.error);
            assert.equal(result.message, 'someString');
        });

        it('should not add a product to the cart because of maxOrderQuantity attribute', function () {
            var currentBasket = createApiBasket();

            var result = cartHelpers.addProductToCart(currentBasket, 'checkMaxQtyProduct', 4, [], mockOptions);
            assert.isTrue(result.error);
            assert.equal(result.message, 'someString');
        });

        it('should not add a product to the cart because of stock threshold Product attribute', function () {
            var currentBasket = createApiBasket();

            var result = cartHelpers.addProductToCart(currentBasket, 'checkThresholdQtyProduct', 4, [], mockOptions);
            assert.isTrue(result.error);
            assert.equal(result.message, 'someString');
        });
    });

    describe('cartHelpers core - checkBundledProductCanBeAdded', function () {
        var childProducts = [
            {
                pid: 'someProductID',
                quantity: 1
            },
            {
                pid: 'checkMaxQtyProduct',
                quantity: 1
            }
        ]
        it('should return true', function () {
            var result = cartHelpers.checkBundledProductCanBeAdded(childProducts, [], 2);
            assert.isTrue(result);
        });

        it('should return false because of ATS', function () {
            var result = cartHelpers.checkBundledProductCanBeAdded(childProducts, [], 9);
            assert.equal(result, false);
        });

        it('should return false because of maxOrderQuantity product attribute', function () {
            var result = cartHelpers.checkBundledProductCanBeAdded(childProducts, [], 4);
            assert.equal(result, false);
        });
    });
});
