var assert = require('chai').assert;
var searchHelperPath = '../../../../../cartridges/app_atg_core/cartridge/scripts/helpers/searchHelpers';
var mockSuperModule = require('../../../../mockModuleSuperModule');
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();
var sinon = require('sinon');
var baseSearchHelpersMock = require('../../../../../test/mocks/scripts/helpers/baseSearchHelpers');

describe('search helpers', function () {
    describe('search', function () {
        var productSearchStub = sinon.stub();
        var searchSpy = sinon.spy();
        var searchHelpers;
        before(function () {
            mockSuperModule.create(baseSearchHelpersMock);

            searchHelpers = proxyquire(searchHelperPath, {
                'dw/catalog/CatalogMgr': {
                    getSortingOptions: function () {
                        return;
                    },
                    getSiteCatalog: function () {
                        return { getRoot: function () { return; } };
                    },
                    getSortingRule: function (rule) {
                        return rule;
                    },
                    getCategory: function () {
                        return { ID: 'mens', online: true };
                    }
                },
                'dw/catalog/ProductSearchModel': function () {
                    return {
                        search: searchSpy,
                        getSearchRedirect: function () {
                            return {
                                getLocation: function () {
                                    return 'some value';
                                }
                            };
                        },
                        category: {
                            parent: {
                                ID: 'root'
                            },
                            template: 'rendering/category/categoryproducthits',
                            custom: {
                                isHidden: false
                            }
                        }
                    };
                },
                'dw/web/URLUtils': {
                    url: function () {
                        return {
                            append: function () {
                                return 'some appened URL';
                            }
                        };
                    }
                },
                '*/cartridge/scripts/helpers/pageMetaHelper': {
                    setPageMetaTags: function () {
                        return;
                    },
                    setPageMetaData: function () {
                        return;
                    }
                },
                '*/cartridge/scripts/helpers/structuredDataHelper': {
                    getListingPageSchema: function () {
                        return 'some schema';
                    }
                },
                '*/cartridge/models/search/productSearch': productSearchStub,
                '*/cartridge/scripts/reportingUrls': {
                    getProductSearchReportingURLs: function () {
                        return ['something', 'something else'];
                    }
                },
                '*/cartridge/scripts/search/search': {
                    setProductProperties: function () {
                        return;
                    },
                    addRefinementValues: function () {
                        return;
                    }
                },
                '*/cartridge/config/preferences': function() {
                    return {
                        maxOrderQty: 6,
                        defaultPageSize: 24,
                        storefrontProductPriceThresholdEnabled: false,
                        storefrontProductPriceThreshold:  0
                    }
                }
            });
        });
        after(function () {
            mockSuperModule.remove();
        });

        var res = {
            cachePeriod: '',
            cachePeriodUnit: '',
            personalized: false,
            getViewData: function () {
                return {};
            }
        };
        var mockRequest1 = {
            querystring: {}
        };
        var mockRequest2 = { querystring: { q: 'someValue' } };
        var mockRequest3 = { querystring: { cgid: 'someCategory', preferences: 'preferences', pmin: 'pmin', pmax: 'pmax' } };

        afterEach(function () {
            productSearchStub.reset();
        });

        it('should category search', function () {
            productSearchStub.returns({
                isCategorySearch: true,
                isRefinedCategorySearch: false
            });
            var result = searchHelpers.search(mockRequest1, res);

            assert.equal(result.maxSlots, 4);
            assert.equal(result.categoryTemplate, 'rendering/category/categoryproducthits');
            assert.equal(result.reportingURLs.length, 2);
            assert.isDefined(result.canonicalUrl);
            assert.isDefined(result.schemaData);
        });

        it('should search', function () {
            productSearchStub.returns({
                isCategorySearch: false,
                isRefinedCategorySearch: false
            });

            var result = searchHelpers.search(mockRequest1, res);

            assert.equal(result.maxSlots, 4);
            assert.equal(result.category, null);
            assert.equal(result.categoryTemplate, null);
            assert.equal(result.reportingURLs.length, 2);
        });

        // it('should get a search redirect url', function () {
        //     var result = searchHelpers.search(mockRequest2, res);

        //     assert.equal(result.searchRedirect, 'some value');
        //     assert.isTrue(searchSpy.notCalled);
        //     assert.equal(result.maxSlots, null);
        // });
    });
});
