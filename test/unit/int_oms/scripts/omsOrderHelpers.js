'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noCallThru().noPreserveCache();



describe('oms back objects', function () {
    var omsOrderHelper = proxyquire('../../../../cartridges/int_oms/cartridge/scripts/helpers/omsOrderHelpers', {
        'dw/order/OrderMgr': {},
        'dw/value/Money': {},
        'dw/order/PaymentInstrument': {},
        'dw/order/PaymentMgr': {},
        'dw/util/StringUtils': { formatMoney: null },
        'dw/web/Resource': {},
        'dw/web/URLUtils': {},
        'dw/util/ArrayList': {},
        'dw/system/Site': { current: null },
        'dw/util/Calendar': {},
        'dw/util/StringUtils': {},
        'dw/system/Logger': { getLogger: function (type) { } },
        '*/cartridge/scripts/util/omsConstants': {},
        '*/cartridge/scripts/helpers/paymentMethodHelpers': {},
        '*/cartridge/scripts/helpers/invoiceHelpers': {},
        '*/cartridge/scripts/util/collections': {}
    });

    var omsProductLineItem = {
        "_type": "product_item",
        "adjusted_tax": 80.95,
        "base_price": 425,
        "bonus_product_line_item": false,
        "gift": false,
        "item_id": "c35d729d62cac27414faea7494",
        "item_text": "Buado Bikini Bottoms",
        "price": 1700,
        "price_after_item_discount": 1700,
        "price_after_order_discount": 1700,
        "product_id": "213374047",
        "product_name": "Buado Bikini Bottoms",
        "quantity": 4,
        "shipment_id": "demo00065507",
        "tax": 80.95,
        "tax_basis": 1700,
        "tax_class_id": "standard",
        "tax_rate": 0.05,
        "c_cancellable_ind": true,
        "c_exchangeable_ind": true,
        "c_isfurniture": false,
        "c_regular_price": 425,
        "c_returnable_ind": true,
        "brand": "Juan De Dios",
        "variation_attributes": [
            {
                "display_value": "Black/White",
                "display_name": "Color"
            },
            {
                "display_value": "XS",
                "display_name": "Size"
            }
        ],
        "images": {
            "swatch": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "title": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dw90ff0804/images/hi-res/BLM/Juan De Dios/213374048/213374048_SW.jpg?sw=80&sh=120&q=50"
                }
            ],
            "large": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "title": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg?sw=800&sh=1200&q=50"
                }
            ],
            "hi-res": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "title": "Buado Bikini Bottoms",
                    "url": "https://demo-eu01-bloomingdales.demandware.net/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg"
                }
            ]
        }
    }
    var returnOmsObject = {
        "Color": "Black/White",
        "Size": "XS",
        "brand": "Juan De Dios",
        "title": "Buado Bikini Bottoms",
        "images": {
            "large": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg?sw=800&sh=1200&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "hi-res": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://demo-eu01-bloomingdales.demandware.net/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "swatch": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dw90ff0804/images/hi-res/BLM/Juan De Dios/213374048/213374048_SW.jpg?sw=80&sh=120&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ]
        },
        "quantity": 2,
        "product_id": "213374047",
        "reason_code": "R052",
        "shipment_id": "demo00065507",
        "product_name": "Buado Bikini Bottoms",
        "variation_attributes": [
            {
                "display_name": "Color",
                "display_value": "Black/White"
            },
            {
                "display_name": "Size",
                "display_value": "XS"
            }
        ]
    }
    var cancelOmsObject = {
        "Color": "Black/White",
        "Size": "XS",
        "brand": "Juan De Dios",
        "title": "Buado Bikini Bottoms",
        "images": {
            "large": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg?sw=800&sh=1200&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "hi-res": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://demo-eu01-bloomingdales.demandware.net/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "swatch": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dw90ff0804/images/hi-res/BLM/Juan De Dios/213374048/213374048_SW.jpg?sw=80&sh=120&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ]
        },
        "quantity": 2,
        "product_id": "213374047",
        "reason_code": "R052",
        "shipment_id": "demo00065507",
        "product_name": "Buado Bikini Bottoms",
        "variation_attributes": [
            {
                "display_name": "Color",
                "display_value": "Black/White"
            },
            {
                "display_name": "Size",
                "display_value": "XS"
            }
        ]
    }

    var exchangeOmsObject = {
        "Color": "Black/White",
        "Size": "XS",
        "brand": "Juan De Dios",
        "title": "Buado Bikini Bottoms",
        "images": {
            "large": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg?sw=800&sh=1200&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "hi-res": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://demo-eu01-bloomingdales.demandware.net/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dwef94e259/images/hi-res/BLM/Juan De Dios/213374048/213374048_IN.jpg",
                    "title": "Buado Bikini Bottoms"
                }
            ],
            "swatch": [
                {
                    "alt": "Buado Bikini Bottoms",
                    "url": "https://edge.disstg.commercecloud.salesforce.com/dw/image/v2/BDSP_DEV/on/demandware.static/-/Sites-bloomingdales-master-catalog/default/dw90ff0804/images/hi-res/BLM/Juan De Dios/213374048/213374048_SW.jpg?sw=80&sh=120&q=50",
                    "title": "Buado Bikini Bottoms"
                }
            ]
        },
        "quantity": 2,
        "product_id": "213374047",
        "exchange_item": "213440085",
        "shipment_id": "demo00065507",
        "product_name": "Buado Bikini Bottoms",
        "variation_attributes": [
            {
                "display_name": "Color",
                "display_value": "Black/White"
            },
            {
                "display_name": "Size",
                "display_value": "XS"
            }
        ]
    }

    it('should create a return object to send oms from object that is taken from oms again', function () {
        var returnObject = omsOrderHelper.createOMSObjectForReturnCancelExchange('return', omsProductLineItem, { reason_code: 'R052', quantity: 2 })[0];
        assert.deepEqual(returnObject, returnOmsObject);
    });

    it('should create a return object to send oms from object that is taken from oms again', function () {
        var cancelObject = omsOrderHelper.createOMSObjectForReturnCancelExchange('cancel', omsProductLineItem, { reason_code: 'R052', quantity: 2 })[0];
        assert.deepEqual(cancelObject, cancelOmsObject);
    });

    it('should create a return object to send oms from object that is taken from oms again', function () {
        var exchangeObject = omsOrderHelper.createOMSObjectForReturnCancelExchange('exchange', omsProductLineItem, { exchange_item: '213440085', quantity: 2 })[0];
        assert.deepEqual(exchangeObject, exchangeOmsObject);
    });

    it('should create a return object to send oms from object that is taken from oms again', function () {
        var exchangeObject = omsOrderHelper.createOMSObjectForReturnCancelExchange('test', omsProductLineItem, { exchange_item: '213440085', quantity: 2 });
        assert.equal(exchangeObject.length, 0);
    });

});

