'use strict';

export const createAccountTab = '.b-login-page__body .nav-item:nth-child(2)';
export const logInTab = '.b-login-page__body .nav-item:nth-child(1)';
export const createAccountButton = '#register .btn-primary';
export const getRegisterFormFeedback = '#register .form-control-feedback';
export const getTrackOrderFormFeedback = '.trackorder .form-control-feedback';
export const checkStatusButton = '.trackorder .btn-primary';
