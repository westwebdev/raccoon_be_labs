'use strict';
var sinon = require('sinon');

var BaseSearchHelpers = {
    setupSearch: function () {
        return {
            search: sinon.spy(),
            getSearchRedirect: function () {
                return {
                    getLocation: function () {
                        return 'some value';
                    }
                };
            },
            category: {
                parent: {
                    ID: 'root'
                },
                template: 'rendering/category/categoryproducthits',
                custom: {
                    isHidden: false
                }
            }
        };
    },
    getCategoryTemplate: function () {
        return 'rendering/category/categoryproducthits';
    },
    setupContentSearch: function () {},
    search: function () {},
    applyCache: function () {}
};

module.exports = BaseSearchHelpers;
