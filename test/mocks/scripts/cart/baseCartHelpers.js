'use strict';

var BaseCartHelpers = {
    addLineItem: function (currentBasket,
                           product,
                           quantity,
                           childProducts,
                           optionModel,
                           defaultShipment) {
        var productLineItem = currentBasket.createProductLineItem(product,
            optionModel,
            defaultShipment);
        productLineItem.setQuantityValue(quantity);
        return productLineItem;
    },
    getQtyAlreadyInCart: function () { return 1; },
    getExistingProductLineItemsInCart: function () { return []; },
    hasSameOptions: function () { return true; },
    allBundleItemsSame: function () { return; },
    getAllProductLineItems: function (pid) { 
        return {
            currentIndex: 0,
            iterator: function () {
                var data = [{
                    productId: pid, 
                    custom: { 
                        omsTotalStockOnHand: null 
                    } 
                }];
                return {
                    hasNext: function () { 
                        return this.currentIndex < data.length; 
                    },
                    next: function () {
                        return data[this.currentIndex++];
                    }
                }
            }, 
        }; 
    }
};

module.exports = BaseCartHelpers;
