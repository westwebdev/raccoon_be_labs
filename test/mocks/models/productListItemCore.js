'use strict';

var proxyquire = require('proxyquire').noCallThru().noPreserveCache();

var cartHelpersCommon = require('../helpers/cartHelpers_common.js');

var productImagesMock = function () {
    return {
        product: 'some product image'
    };
};

var priceFactoryMock = {
    getPrice: function () {
        return {
            sales: {
                decimalPrice: 200
            },
            list: {
                decimalPrice: 250
            }
        };
    }
};

var PromotionMgrMock = {
    activeCustomerPromotions: {
        getProductPromotions: function () {
            return {
                name: 'promoName'
            };
        }
    }
};

var urlUtils = {
    url: function (endPointName) {
        return {
            toString: function () {
                return 'path-to-endpoint/' + endPointName;
            }
        };
    }
};

function proxyModel() {
    return proxyquire('../../../cartridges/app_atg_core/cartridge/models/productListItem', {
        '*/cartridge/models/product/productImages': productImagesMock,
        '*/cartridge/scripts/factories/price': priceFactoryMock,
        '*/cartridge/models/product/decorators/quantity': function () {},
        'dw/campaign/PromotionMgr': PromotionMgrMock,
        '*/cartridge/models/product/decorators/availability': function () {},
        '*/cartridge/models/product/decorators/variationAttributes': function () {},
        '*/cartridge/models/product/decorators/readyToOrder': function () {},
        '*/cartridge/config/preferences': {
            maxOrderQty: 10
        },
        '*/cartridge/scripts/helpers/productHelpers': {
            getOptions: cartHelpersCommon.getOptions,
            getCurrentOptionModel: cartHelpersCommon.getCurrentOptionModel,
            setInitialAttrValues: function () {},
            getConfig: function () {
                return {
                    promotions: null
                }
            }
        },
        'dw/web/URLUtils': urlUtils,
        '*/cartridge/models/product/fullProduct': function () {
            return {
                productID: 'some pid',
                UUID: 'some UUID',
                isBeauty: false,
                product: {
                    name: 'some productName',
                    master: 'some productMaster',
                    bundle: false,
                    minOrderQuantity: {
                        value: 'product minOrderQuantity'
                    },
                    availabilityModel: {
                        inventoryRecord: {
                            ATS: {
                                value: 10
                            }
                        }
                    },
                    master: true,
                    primaryCategory: {
                        custom: {
                            isBeauty: false
                        }
                    }
                },
                custom: {
                    wishlistPrice: 200
                },
                quantityValue: 2,
                public: 'some PublicItem',
                getLastModified: function () {
                    return {
                        getTime: function () {
                            return '1527213625';
                        }
                    };
                },
                getCreationDate: function () {
                    return {
                        getTime: function () {
                            return '1527213655';
                        }
                    };
                }
            }
        }
    });
}

module.exports = proxyModel();
