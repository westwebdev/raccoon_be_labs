const options = require('./options');
var exec = require('child_process').exec;

const coreCartridge = 'app_atg_core';

var fontConfig = [
    `cartridges/${options.site}/cartridge/static/default/images/icons/*.svg`,
    '--font-name iconfont',
    `-d cartridges/${options.site}/cartridge/static/default/fonts/icons`,
    `-t cartridges/${coreCartridge}/cartridge/client/default/scss/_api/util/icons/_util-icons.scss.njk`,
    `-s cartridges/${options.site}/cartridge/client/default/scss/_api/util/icons/`,
    '-c b-icon',
    '-p ../../fonts/icons',
    '--centerHorizontally',
    '--normalize'
];

exec(`mkdirp cartridges/${options.site}/cartridge/static/default/fonts/icons`);
exec("webfont " + fontConfig.join(' '));
