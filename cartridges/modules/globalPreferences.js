/**
 * Organization preferences module
 * @module globalPreferences
 */
module.exports = {
    /**
    * Attribute to store already retreived values
    * @private
    */
    results: {},

    /**
    * @private
    */
    Logger: require('dw/system').Logger.getLogger('globalpreferences', 'error'),

    /**
    * Simple function which returns organization preferences value
    * @param {string} key - ID of the custom preference attribute
    * @param {*} [defaults] - Default value to return if given preference is missing
    * @return {*} - The preference value or default one
    */
    getValue: function (key, defaults) {
        if (typeof key !== 'string') {
            return defaults;
        }
		if (!(key in this.results)) {
			var result = require('dw/system').System.getPreferences().getCustom()[key];
			this.results[key] = result === null ? defaults : result;
		}
		return this.results[key];
    },

    /**
    * Simple function which returns organization enum preference values
    * @param {string} key - ID of the custom preference attribute
    * @param {*} [defaults] - Default value to return if given preference is missing
    * @return {Array} - Enum values converted to array
    */
    getEnumValues: function (key, defaults) {
        if (typeof key !== 'string') {
            return defaults;
        }
        var result = require('dw/system').System.getPreferences().getCustom()[key];
        var resultArr = [];
        if (!empty(result)) {
            Object.keys(result).forEach(function (prop) {
                resultArr.push(result[prop].value);
            });
        }

        return empty(resultArr) ? defaults : resultArr;
    },

    /**
    * Function which returns site preference value based on JSON
    * @param {string} query - Point separated path to certain attribute inside JSON. First part should be preference ID.
    * @param {*} [defaults] - Default value to return if given preference is missing or JSON is invalid
    * @return {*} - The preference value or default one
    */
    getJsonPreference: function (query, defaults) {
        if (!(query in this.results)) {
            var queryObject = query.split('.');
            var preferenceJsonValue = require('dw/system').System.getPreferences().getCustom()[queryObject[0]];
            var PreferenceValue = '';

            if (!preferenceJsonValue) {
                return defaults;
            }

            try {
                PreferenceValue = JSON.parse(preferenceJsonValue);
            } catch (e) {
                this.Logger.error('JSON Organization custom preferences ' + queryObject[0] + ' has wrong format');
                return defaults;
            }
            for (var i = 1; i < queryObject.length; i += 1) {
                if (PreferenceValue[queryObject[i]]) {
                    PreferenceValue = PreferenceValue[queryObject[i]];
                } else {
                    this.Logger.debug('Cannot get JSON preference value: there is no '
                        + queryObject[i] + ' in ' + queryObject[i - 1]);
                    return defaults;
                }
            }
            this.results[query] = PreferenceValue;
        }
        return this.results[query];
    }
};
