/**
 * Common devices module. Use it only for pages that are not cached
 * @module devices
 */

module.exports = {
    /**
     * Defines whether current device is mobile
     * @param  {string} userAgent
     * @return {boolean} true if current device is mobile
     */
	isMobile: function (userAgent) {
		var isMobile = false;

		if (!empty(userAgent)) {
			isMobile = !!userAgent.toLowerCase().match(new RegExp('(mobile|phone|ipod|android|blackberry|windows\ ce|opera\ mini|palm)'));
		}
		return isMobile;
	},

    /**
     * Defines whether current device is tablet
     * @param  {string} userAgent
     * @return {boolean} true if current device is tablet
     */
	isTablet: function (userAgent) {
		var isTablet = false;

		if (!empty(userAgent)) {
			userAgent = userAgent.toLowerCase();
			if (userAgent.indexOf('ipad') > -1) {
				isTablet = true;
			} else if (userAgent.indexOf('mobile') === -1) {
				if (userAgent.indexOf('android') > -1) {
					isTablet = true;
				} else if (userAgent.indexOf('bb') > -1 && userAgent.indexOf('touch') > -1) {
					isTablet = true;
				} else if (userAgent.indexOf('blackberry') > -1) {
					isTablet = true;
				}
			}
		}

		return isTablet;
	},


    /**
     * Defines whether current device is desktop
     * @param  {string} userAgent
     * @return {boolean} true if current device is dekstop
     */
	isDesktop: function (userAgent) {
		var agent = (userAgent || request.httpUserAgent || '').toLowerCase();
		return !this.isMobile(agent) && !this.isTablet(agent);
	},

    /**
     * Detects device type
     * @param  {string} userAgent
     * @return {string} device type
     */
	getDeviceType: function (userAgent) {
		var deviceType = 'desktop';
		if (this.isMobile(userAgent)) {
			deviceType = 'mobile';
		} else if (this.isTablet(userAgent)) {
			deviceType = 'tablet';
		}
		return deviceType;
	}
};