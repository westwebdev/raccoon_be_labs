
'use strict';

/*
 * Library provides helpful methods for working with Custom Caches
 */

function generateKey(key, args, options) {
    var opts = options || {};
    var site = opts.isSiteSpecific ? require('dw/system/Site').getCurrent().getID() : '';
    var locale = opts.isLocaleSpecific ? request.getLocale() : '';
    return key + args.join('') + site + locale;
}

/**
 * Wraps loader function with caching decorator using given cache ID, key and options
 * @param {string} cacheID - ID of the cache
 * @param {string} key - cache key
 * @param {Function} loader - loader function that is called if no value is stored in the cache
 * @param {Object} options - additional options used to generate cache key
 * @returns {Function} - decorator which implements caching above the loader function
 */
function cache(cacheID, key, loader, options) {
    var CacheMgr = require('dw/system/CacheMgr');
    return function(partitionIndex) {
        var cache = CacheMgr.getCache(cacheID);
        var args = [].slice.apply(arguments);
        var self = this;
        var cacheKey = key + (partitionIndex || '');
        return cache.get(generateKey(cacheKey, args, options), function () {
            return loader.apply(self, args);
        });
    };
}

module.exports = {
    cache: cache
};