module.exports = {
    logger: require('dw/system').Logger.getLogger('cookie', 'cookie'),

    /**
     * Getting a cookie from the HTTP request
     *
     * @param {string} name Cookie name
     * @param {CurrentRequest} currentRequest Current request instance
     * @param {Object} options Additional options
     * @return {string} Cookie value
     */
    get: function (name, currentRequest, options) {
        var cookies = currentRequest.getHttpCookies();
        var result = null;

        for (var i = 0, l = cookies.cookieCount; i < l; i += 1) {
            var cookie = cookies[i];
            if (!empty(cookie) && cookie.name.equals(name)) {
                result = cookie.value;
                break;
            }
        }

        if (!empty(options) && 'splitByComma' in options && options.splitByComma === true) {
            if (empty(result)) return [];
            result = result.split(',');
        }

        return result;
    },

    /**
     * Getting a JSON cookie from the HTTP request
     *
     * @param {string} name Cookie name
     * @param {CurrentRequest} currentRequest Current request instance
     * @param {*} [defaults] - Default value to return if given cookie is missing or JSON is invalid
     * @return {string} Cookie value
     */
    getJSONCookie: function (name, currentRequest, defaults) {
        var cookieValue = this.get(name, currentRequest);
        var parsed = defaults;
        if (!empty(cookieValue)) {
            cookieValue = require('dw/crypto/Encoding').fromURI(cookieValue);
            try {
                parsed = JSON.parse(cookieValue);
            } catch(e) {
                this.logger.error('JSON cookie ' + name + ' has wrong format');
            }
        }

        return parsed;
    },

    /**
     * Setting a cookie in the HTTP request
     *
     * @param {string} name Cookie name
     * @param {string} value Cookie value
     * @param {CurrentResponse} currentResponse Current request instance
     * @param {number} maxAge Lifetime of the cookie in seconds
     * @param {string} path Cookie path
     * @param {string} domain Cookie domain
     * @return {string} Cookie value
     */
    set: function (name, value, currentResponse, maxAge, path, domain) {
        var cookie = new (require('dw/web/Cookie'))(name, value);

        cookie.setMaxAge(maxAge || -1);
        if (path) {
            cookie.setPath(path);
        }
        if (domain) {
            cookie.setDomain(domain);
        }
        currentResponse.addHttpCookie(cookie);

        return value;
    },

    /**
     * Removes a cookie with specified names from the HTTP response
     *
     * @param {string} name Cookie name
     * @param {dw.system.Response} currentResponse current response instance
     * @param {string} path Cookie path
     */
    remove: function (name, currentResponse, path) {
        var cookie = new (require('dw/web/Cookie'))(name, 'def');
        if (path) {
            cookie.setPath(path);
        }
        cookie.setMaxAge(0);
        currentResponse.addHttpCookie(cookie);
    }
};
