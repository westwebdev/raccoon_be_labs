/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar processInclude = __webpack_require__(/*! ./util */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js\");\n\n$(document).ready(function () {\n  processInclude(__webpack_require__(/*! ./addressBook/addressBook */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook/addressBook.js\"));\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvYWRkcmVzc0Jvb2suanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvYWRkcmVzc0Jvb2suanM/MmNjMyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBwcm9jZXNzSW5jbHVkZSA9IHJlcXVpcmUoJy4vdXRpbCcpO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICAgcHJvY2Vzc0luY2x1ZGUocmVxdWlyZSgnLi9hZGRyZXNzQm9vay9hZGRyZXNzQm9vaycpKTtcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook/addressBook.js":
/*!****************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook/addressBook.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar formValidation = __webpack_require__(/*! ../components/formValidation */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\");\n\nvar url;\nvar isDefault;\n/**\n * Create an alert to display the error message\n * @param {Object} message - Error message to display\n */\n\nfunction createErrorNotification(message) {\n  var errorHtml = '<div class=\"alert alert-danger alert-dismissible valid-cart-error ' + 'fade show\" role=\"alert\">' + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">' + '<span aria-hidden=\"true\">&times;</span>' + '</button>' + message + '</div>';\n  $('.error-messaging').append(errorHtml);\n}\n\nmodule.exports = {\n  removeAddress: function removeAddress() {\n    $('.remove-address').on('click', function (e) {\n      e.preventDefault();\n      isDefault = $(this).data('default');\n\n      if (isDefault) {\n        url = $(this).data('url') + '?addressId=' + $(this).data('id') + '&isDefault=' + isDefault;\n      } else {\n        url = $(this).data('url') + '?addressId=' + $(this).data('id');\n      }\n\n      $('.product-to-remove').empty().append($(this).data('id'));\n    });\n  },\n  removeAddressConfirmation: function removeAddressConfirmation() {\n    $('.delete-confirmation-btn').click(function (e) {\n      e.preventDefault();\n      $.ajax({\n        url: url,\n        type: 'get',\n        dataType: 'json',\n        success: function success(data) {\n          $('#uuid-' + data.UUID).remove();\n\n          if (isDefault) {\n            var addressId = $('.card .address-heading').first().text();\n            var addressHeading = addressId + ' (' + data.defaultMsg + ')';\n            $('.card .address-heading').first().text(addressHeading);\n            $('.card .card-make-default-link').first().remove();\n            $('.remove-address').data('default', true);\n\n            if (data.message) {\n              var toInsert = '<div><h3>' + data.message + '</h3><div>';\n              $('.addressList').after(toInsert);\n            }\n          }\n        },\n        error: function error(err) {\n          if (err.responseJSON.redirectUrl) {\n            window.location.href = err.responseJSON.redirectUrl;\n          } else {\n            createErrorNotification(err.responseJSON.errorMessage);\n          }\n\n          $.spinner().stop();\n        }\n      });\n    });\n  },\n  submitAddress: function submitAddress() {\n    $('form.address-form').submit(function (e) {\n      var $form = $(this);\n      e.preventDefault();\n      url = $form.attr('action');\n      $form.spinner().start();\n      $('form.address-form').trigger('address:submit', e);\n      $.ajax({\n        url: url,\n        type: 'post',\n        dataType: 'json',\n        data: $form.serialize(),\n        success: function success(data) {\n          $form.spinner().stop();\n\n          if (!data.success) {\n            formValidation($form, data);\n          } else {\n            location.href = data.redirectUrl;\n          }\n        },\n        error: function error(err) {\n          if (err.responseJSON.redirectUrl) {\n            window.location.href = err.responseJSON.redirectUrl;\n          }\n\n          $form.spinner().stop();\n        }\n      });\n      return false;\n    });\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvYWRkcmVzc0Jvb2svYWRkcmVzc0Jvb2suanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvYWRkcmVzc0Jvb2svYWRkcmVzc0Jvb2suanM/MDcwMSJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBmb3JtVmFsaWRhdGlvbiA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvZm9ybVZhbGlkYXRpb24nKTtcblxudmFyIHVybDtcbnZhciBpc0RlZmF1bHQ7XG5cbi8qKlxuICogQ3JlYXRlIGFuIGFsZXJ0IHRvIGRpc3BsYXkgdGhlIGVycm9yIG1lc3NhZ2VcbiAqIEBwYXJhbSB7T2JqZWN0fSBtZXNzYWdlIC0gRXJyb3IgbWVzc2FnZSB0byBkaXNwbGF5XG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZUVycm9yTm90aWZpY2F0aW9uKG1lc3NhZ2UpIHtcbiAgICB2YXIgZXJyb3JIdG1sID0gJzxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC1kYW5nZXIgYWxlcnQtZGlzbWlzc2libGUgdmFsaWQtY2FydC1lcnJvciAnICtcbiAgICAgICAgJ2ZhZGUgc2hvd1wiIHJvbGU9XCJhbGVydFwiPicgK1xuICAgICAgICAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGRhdGEtZGlzbWlzcz1cImFsZXJ0XCIgYXJpYS1sYWJlbD1cIkNsb3NlXCI+JyArXG4gICAgICAgICc8c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIj4mdGltZXM7PC9zcGFuPicgK1xuICAgICAgICAnPC9idXR0b24+JyArIG1lc3NhZ2UgKyAnPC9kaXY+JztcblxuICAgICQoJy5lcnJvci1tZXNzYWdpbmcnKS5hcHBlbmQoZXJyb3JIdG1sKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgcmVtb3ZlQWRkcmVzczogZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCcucmVtb3ZlLWFkZHJlc3MnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgaXNEZWZhdWx0ID0gJCh0aGlzKS5kYXRhKCdkZWZhdWx0Jyk7XG4gICAgICAgICAgICBpZiAoaXNEZWZhdWx0KSB7XG4gICAgICAgICAgICAgICAgdXJsID0gJCh0aGlzKS5kYXRhKCd1cmwnKVxuICAgICAgICAgICAgICAgICAgICArICc/YWRkcmVzc0lkPSdcbiAgICAgICAgICAgICAgICAgICAgKyAkKHRoaXMpLmRhdGEoJ2lkJylcbiAgICAgICAgICAgICAgICAgICAgKyAnJmlzRGVmYXVsdD0nXG4gICAgICAgICAgICAgICAgICAgICsgaXNEZWZhdWx0O1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB1cmwgPSAkKHRoaXMpLmRhdGEoJ3VybCcpICsgJz9hZGRyZXNzSWQ9JyArICQodGhpcykuZGF0YSgnaWQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICQoJy5wcm9kdWN0LXRvLXJlbW92ZScpLmVtcHR5KCkuYXBwZW5kKCQodGhpcykuZGF0YSgnaWQnKSk7XG4gICAgICAgIH0pO1xuICAgIH0sXG5cbiAgICByZW1vdmVBZGRyZXNzQ29uZmlybWF0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJy5kZWxldGUtY29uZmlybWF0aW9uLWJ0bicpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdnZXQnLFxuICAgICAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgJCgnI3V1aWQtJyArIGRhdGEuVVVJRCkucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0RlZmF1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhZGRyZXNzSWQgPSAkKCcuY2FyZCAuYWRkcmVzcy1oZWFkaW5nJykuZmlyc3QoKS50ZXh0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgYWRkcmVzc0hlYWRpbmcgPSBhZGRyZXNzSWQgKyAnICgnICsgZGF0YS5kZWZhdWx0TXNnICsgJyknO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCgnLmNhcmQgLmFkZHJlc3MtaGVhZGluZycpLmZpcnN0KCkudGV4dChhZGRyZXNzSGVhZGluZyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcuY2FyZCAuY2FyZC1tYWtlLWRlZmF1bHQtbGluaycpLmZpcnN0KCkucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcucmVtb3ZlLWFkZHJlc3MnKS5kYXRhKCdkZWZhdWx0JywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5tZXNzYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRvSW5zZXJ0ID0gJzxkaXY+PGgzPicgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLm1lc3NhZ2UgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnPC9oMz48ZGl2Pic7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnLmFkZHJlc3NMaXN0JykuYWZ0ZXIodG9JbnNlcnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyLnJlc3BvbnNlSlNPTi5yZWRpcmVjdFVybCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBlcnIucmVzcG9uc2VKU09OLnJlZGlyZWN0VXJsO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlRXJyb3JOb3RpZmljYXRpb24oZXJyLnJlc3BvbnNlSlNPTi5lcnJvck1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICQuc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIHN1Ym1pdEFkZHJlc3M6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnZm9ybS5hZGRyZXNzLWZvcm0nKS5zdWJtaXQoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICQodGhpcyk7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB1cmwgPSAkZm9ybS5hdHRyKCdhY3Rpb24nKTtcbiAgICAgICAgICAgICRmb3JtLnNwaW5uZXIoKS5zdGFydCgpO1xuICAgICAgICAgICAgJCgnZm9ybS5hZGRyZXNzLWZvcm0nKS50cmlnZ2VyKCdhZGRyZXNzOnN1Ym1pdCcsIGUpO1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICB0eXBlOiAncG9zdCcsXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgICAgICBkYXRhOiAkZm9ybS5zZXJpYWxpemUoKSxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAkZm9ybS5zcGlubmVyKCkuc3RvcCgpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybVZhbGlkYXRpb24oJGZvcm0sIGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYXRpb24uaHJlZiA9IGRhdGEucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIucmVzcG9uc2VKU09OLnJlZGlyZWN0VXJsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGVyci5yZXNwb25zZUpTT04ucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgJGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgfVxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUEzQkE7QUE2QkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBbEJBO0FBb0JBO0FBQ0E7QUFDQTtBQWxGQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/addressBook/addressBook.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js":
/*!******************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n/**\n * Remove all validation. Should be called every time before revalidating form\n * @param {element} form - Form to be cleared\n * @returns {void}\n */\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction clearFormErrors(form) {\n  $(form).find('.form-control.is-invalid').removeClass('is-invalid');\n}\n\nmodule.exports = function (formElement, payload) {\n  // clear form validation first\n  clearFormErrors(formElement);\n  $('.alert', formElement).remove();\n\n  if (_typeof(payload) === 'object' && payload.fields) {\n    Object.keys(payload.fields).forEach(function (key) {\n      if (payload.fields[key]) {\n        var feedbackElement = $(formElement).find('[name=\"' + key + '\"]').parent().children('.invalid-feedback');\n\n        if (feedbackElement.length > 0) {\n          if (Array.isArray(payload[key])) {\n            feedbackElement.html(payload.fields[key].join('<br/>'));\n          } else {\n            feedbackElement.html(payload.fields[key]);\n          }\n\n          feedbackElement.siblings('.form-control').addClass('is-invalid');\n        }\n      }\n    });\n  }\n\n  if (payload && payload.error) {\n    var form = $(formElement).prop('tagName') === 'FORM' ? $(formElement) : $(formElement).parents('form');\n    form.prepend('<div class=\"alert alert-danger\" role=\"alert\">' + payload.error.join('<br/>') + '</div>');\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY29tcG9uZW50cy9mb3JtVmFsaWRhdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy9jb21wb25lbnRzL2Zvcm1WYWxpZGF0aW9uLmpzPzdhY2YiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFJlbW92ZSBhbGwgdmFsaWRhdGlvbi4gU2hvdWxkIGJlIGNhbGxlZCBldmVyeSB0aW1lIGJlZm9yZSByZXZhbGlkYXRpbmcgZm9ybVxuICogQHBhcmFtIHtlbGVtZW50fSBmb3JtIC0gRm9ybSB0byBiZSBjbGVhcmVkXG4gKiBAcmV0dXJucyB7dm9pZH1cbiAqL1xuZnVuY3Rpb24gY2xlYXJGb3JtRXJyb3JzKGZvcm0pIHtcbiAgICAkKGZvcm0pLmZpbmQoJy5mb3JtLWNvbnRyb2wuaXMtaW52YWxpZCcpLnJlbW92ZUNsYXNzKCdpcy1pbnZhbGlkJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZvcm1FbGVtZW50LCBwYXlsb2FkKSB7XG4gICAgLy8gY2xlYXIgZm9ybSB2YWxpZGF0aW9uIGZpcnN0XG4gICAgY2xlYXJGb3JtRXJyb3JzKGZvcm1FbGVtZW50KTtcbiAgICAkKCcuYWxlcnQnLCBmb3JtRWxlbWVudCkucmVtb3ZlKCk7XG5cbiAgICBpZiAodHlwZW9mIHBheWxvYWQgPT09ICdvYmplY3QnICYmIHBheWxvYWQuZmllbGRzKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKHBheWxvYWQuZmllbGRzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLmZpZWxkc1trZXldKSB7XG4gICAgICAgICAgICAgICAgdmFyIGZlZWRiYWNrRWxlbWVudCA9ICQoZm9ybUVsZW1lbnQpLmZpbmQoJ1tuYW1lPVwiJyArIGtleSArICdcIl0nKVxuICAgICAgICAgICAgICAgICAgICAucGFyZW50KClcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKCcuaW52YWxpZC1mZWVkYmFjaycpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGZlZWRiYWNrRWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHBheWxvYWRba2V5XSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZlZWRiYWNrRWxlbWVudC5odG1sKHBheWxvYWQuZmllbGRzW2tleV0uam9pbignPGJyLz4nKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuaHRtbChwYXlsb2FkLmZpZWxkc1trZXldKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuc2libGluZ3MoJy5mb3JtLWNvbnRyb2wnKS5hZGRDbGFzcygnaXMtaW52YWxpZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGlmIChwYXlsb2FkICYmIHBheWxvYWQuZXJyb3IpIHtcbiAgICAgICAgdmFyIGZvcm0gPSAkKGZvcm1FbGVtZW50KS5wcm9wKCd0YWdOYW1lJykgPT09ICdGT1JNJ1xuICAgICAgICAgICAgPyAkKGZvcm1FbGVtZW50KVxuICAgICAgICAgICAgOiAkKGZvcm1FbGVtZW50KS5wYXJlbnRzKCdmb3JtJyk7XG5cbiAgICAgICAgZm9ybS5wcmVwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtZGFuZ2VyXCIgcm9sZT1cImFsZXJ0XCI+J1xuICAgICAgICAgICAgKyBwYXlsb2FkLmVycm9yLmpvaW4oJzxici8+JykgKyAnPC9kaXY+Jyk7XG4gICAgfVxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js":
/*!*********************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nmodule.exports = function (include) {\n  if (typeof include === 'function') {\n    include();\n  } else if (_typeof(include) === 'object') {\n    Object.keys(include).forEach(function (key) {\n      if (typeof include[key] === 'function') {\n        include[key]();\n      }\n    });\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy91dGlsLmpzP2U3NTUiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpbmNsdWRlKSB7XG4gICAgaWYgKHR5cGVvZiBpbmNsdWRlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGluY2x1ZGUoKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBpbmNsdWRlID09PSAnb2JqZWN0Jykge1xuICAgICAgICBPYmplY3Qua2V5cyhpbmNsdWRlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgaW5jbHVkZVtrZXldID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgaW5jbHVkZVtrZXldKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn07XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js\n");

/***/ })

/******/ });