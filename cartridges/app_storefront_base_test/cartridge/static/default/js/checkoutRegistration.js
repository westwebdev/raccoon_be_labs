/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/app_storefront_base_test/cartridge/client/default/js/checkoutRegistration.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/checkoutRegistration.js":
/*!*************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/checkoutRegistration.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar formValidation = __webpack_require__(/*! ./components/formValidation */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\");\n\n$(document).ready(function () {\n  $('form.checkout-registration').submit(function (e) {\n    var form = $(this);\n    e.preventDefault();\n    var url = form.attr('action');\n    form.spinner().start();\n    $.ajax({\n      url: url,\n      type: 'post',\n      dataType: 'json',\n      data: form.serialize(),\n      success: function success(data) {\n        form.spinner().stop();\n\n        if (!data.success) {\n          formValidation(form, data);\n        } else {\n          location.href = data.redirectUrl;\n        }\n      },\n      error: function error(err) {\n        if (err.responseJSON.redirectUrl) {\n          window.location.href = err.responseJSON.redirectUrl;\n        }\n\n        form.spinner().stop();\n      }\n    });\n    return false;\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY2hlY2tvdXRSZWdpc3RyYXRpb24uanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY2hlY2tvdXRSZWdpc3RyYXRpb24uanM/ZWVjYyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBmb3JtVmFsaWRhdGlvbiA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9mb3JtVmFsaWRhdGlvbicpO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICAgJCgnZm9ybS5jaGVja291dC1yZWdpc3RyYXRpb24nKS5zdWJtaXQoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgdmFyIGZvcm0gPSAkKHRoaXMpO1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHZhciB1cmwgPSBmb3JtLmF0dHIoJ2FjdGlvbicpO1xuICAgICAgICBmb3JtLnNwaW5uZXIoKS5zdGFydCgpO1xuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICB0eXBlOiAncG9zdCcsXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICAgICAgZGF0YTogZm9ybS5zZXJpYWxpemUoKSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgZm9ybS5zcGlubmVyKCkuc3RvcCgpO1xuICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvcm1WYWxpZGF0aW9uKGZvcm0sIGRhdGEpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGxvY2F0aW9uLmhyZWYgPSBkYXRhLnJlZGlyZWN0VXJsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBlcnJvcjogZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgICAgICAgIGlmIChlcnIucmVzcG9uc2VKU09OLnJlZGlyZWN0VXJsKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gZXJyLnJlc3BvbnNlSlNPTi5yZWRpcmVjdFVybDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZm9ybS5zcGlubmVyKCkuc3RvcCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xufSk7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBbEJBO0FBb0JBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/checkoutRegistration.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js":
/*!******************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n/**\n * Remove all validation. Should be called every time before revalidating form\n * @param {element} form - Form to be cleared\n * @returns {void}\n */\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction clearFormErrors(form) {\n  $(form).find('.form-control.is-invalid').removeClass('is-invalid');\n}\n\nmodule.exports = function (formElement, payload) {\n  // clear form validation first\n  clearFormErrors(formElement);\n  $('.alert', formElement).remove();\n\n  if (_typeof(payload) === 'object' && payload.fields) {\n    Object.keys(payload.fields).forEach(function (key) {\n      if (payload.fields[key]) {\n        var feedbackElement = $(formElement).find('[name=\"' + key + '\"]').parent().children('.invalid-feedback');\n\n        if (feedbackElement.length > 0) {\n          if (Array.isArray(payload[key])) {\n            feedbackElement.html(payload.fields[key].join('<br/>'));\n          } else {\n            feedbackElement.html(payload.fields[key]);\n          }\n\n          feedbackElement.siblings('.form-control').addClass('is-invalid');\n        }\n      }\n    });\n  }\n\n  if (payload && payload.error) {\n    var form = $(formElement).prop('tagName') === 'FORM' ? $(formElement) : $(formElement).parents('form');\n    form.prepend('<div class=\"alert alert-danger\" role=\"alert\">' + payload.error.join('<br/>') + '</div>');\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY29tcG9uZW50cy9mb3JtVmFsaWRhdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy9jb21wb25lbnRzL2Zvcm1WYWxpZGF0aW9uLmpzPzdhY2YiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFJlbW92ZSBhbGwgdmFsaWRhdGlvbi4gU2hvdWxkIGJlIGNhbGxlZCBldmVyeSB0aW1lIGJlZm9yZSByZXZhbGlkYXRpbmcgZm9ybVxuICogQHBhcmFtIHtlbGVtZW50fSBmb3JtIC0gRm9ybSB0byBiZSBjbGVhcmVkXG4gKiBAcmV0dXJucyB7dm9pZH1cbiAqL1xuZnVuY3Rpb24gY2xlYXJGb3JtRXJyb3JzKGZvcm0pIHtcbiAgICAkKGZvcm0pLmZpbmQoJy5mb3JtLWNvbnRyb2wuaXMtaW52YWxpZCcpLnJlbW92ZUNsYXNzKCdpcy1pbnZhbGlkJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZvcm1FbGVtZW50LCBwYXlsb2FkKSB7XG4gICAgLy8gY2xlYXIgZm9ybSB2YWxpZGF0aW9uIGZpcnN0XG4gICAgY2xlYXJGb3JtRXJyb3JzKGZvcm1FbGVtZW50KTtcbiAgICAkKCcuYWxlcnQnLCBmb3JtRWxlbWVudCkucmVtb3ZlKCk7XG5cbiAgICBpZiAodHlwZW9mIHBheWxvYWQgPT09ICdvYmplY3QnICYmIHBheWxvYWQuZmllbGRzKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKHBheWxvYWQuZmllbGRzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLmZpZWxkc1trZXldKSB7XG4gICAgICAgICAgICAgICAgdmFyIGZlZWRiYWNrRWxlbWVudCA9ICQoZm9ybUVsZW1lbnQpLmZpbmQoJ1tuYW1lPVwiJyArIGtleSArICdcIl0nKVxuICAgICAgICAgICAgICAgICAgICAucGFyZW50KClcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKCcuaW52YWxpZC1mZWVkYmFjaycpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGZlZWRiYWNrRWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHBheWxvYWRba2V5XSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZlZWRiYWNrRWxlbWVudC5odG1sKHBheWxvYWQuZmllbGRzW2tleV0uam9pbignPGJyLz4nKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuaHRtbChwYXlsb2FkLmZpZWxkc1trZXldKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuc2libGluZ3MoJy5mb3JtLWNvbnRyb2wnKS5hZGRDbGFzcygnaXMtaW52YWxpZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGlmIChwYXlsb2FkICYmIHBheWxvYWQuZXJyb3IpIHtcbiAgICAgICAgdmFyIGZvcm0gPSAkKGZvcm1FbGVtZW50KS5wcm9wKCd0YWdOYW1lJykgPT09ICdGT1JNJ1xuICAgICAgICAgICAgPyAkKGZvcm1FbGVtZW50KVxuICAgICAgICAgICAgOiAkKGZvcm1FbGVtZW50KS5wYXJlbnRzKCdmb3JtJyk7XG5cbiAgICAgICAgZm9ybS5wcmVwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtZGFuZ2VyXCIgcm9sZT1cImFsZXJ0XCI+J1xuICAgICAgICAgICAgKyBwYXlsb2FkLmVycm9yLmpvaW4oJzxici8+JykgKyAnPC9kaXY+Jyk7XG4gICAgfVxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\n");

/***/ })

/******/ });