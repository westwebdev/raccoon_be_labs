/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./cartridges/app_storefront_base_test/cartridge/client/default/js/login.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/components/errorNotification.js":
/*!*********************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/components/errorNotification.js ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nmodule.exports = function (element, message) {\n  var errorHtml = '<div class=\"alert alert-danger alert-dismissible ' + 'fade show\" role=\"alert\">' + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">' + '<span aria-hidden=\"true\">&times;</span>' + '</button>' + message + '</div>';\n  $(element).append(errorHtml);\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY29tcG9uZW50cy9lcnJvck5vdGlmaWNhdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy9jb21wb25lbnRzL2Vycm9yTm90aWZpY2F0aW9uLmpzP2NkOTQiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChlbGVtZW50LCBtZXNzYWdlKSB7XG4gICAgdmFyIGVycm9ySHRtbCA9ICc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtZGFuZ2VyIGFsZXJ0LWRpc21pc3NpYmxlICcgK1xuICAgICAgICAnZmFkZSBzaG93XCIgcm9sZT1cImFsZXJ0XCI+JyArXG4gICAgICAgICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImNsb3NlXCIgZGF0YS1kaXNtaXNzPVwiYWxlcnRcIiBhcmlhLWxhYmVsPVwiQ2xvc2VcIj4nICtcbiAgICAgICAgJzxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPiZ0aW1lczs8L3NwYW4+JyArXG4gICAgICAgICc8L2J1dHRvbj4nICsgbWVzc2FnZSArICc8L2Rpdj4nO1xuXG4gICAgJChlbGVtZW50KS5hcHBlbmQoZXJyb3JIdG1sKTtcbn07XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/components/errorNotification.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js":
/*!******************************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n/**\n * Remove all validation. Should be called every time before revalidating form\n * @param {element} form - Form to be cleared\n * @returns {void}\n */\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction clearFormErrors(form) {\n  $(form).find('.form-control.is-invalid').removeClass('is-invalid');\n}\n\nmodule.exports = function (formElement, payload) {\n  // clear form validation first\n  clearFormErrors(formElement);\n  $('.alert', formElement).remove();\n\n  if (_typeof(payload) === 'object' && payload.fields) {\n    Object.keys(payload.fields).forEach(function (key) {\n      if (payload.fields[key]) {\n        var feedbackElement = $(formElement).find('[name=\"' + key + '\"]').parent().children('.invalid-feedback');\n\n        if (feedbackElement.length > 0) {\n          if (Array.isArray(payload[key])) {\n            feedbackElement.html(payload.fields[key].join('<br/>'));\n          } else {\n            feedbackElement.html(payload.fields[key]);\n          }\n\n          feedbackElement.siblings('.form-control').addClass('is-invalid');\n        }\n      }\n    });\n  }\n\n  if (payload && payload.error) {\n    var form = $(formElement).prop('tagName') === 'FORM' ? $(formElement) : $(formElement).parents('form');\n    form.prepend('<div class=\"alert alert-danger\" role=\"alert\">' + payload.error.join('<br/>') + '</div>');\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvY29tcG9uZW50cy9mb3JtVmFsaWRhdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy9jb21wb25lbnRzL2Zvcm1WYWxpZGF0aW9uLmpzPzdhY2YiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIFJlbW92ZSBhbGwgdmFsaWRhdGlvbi4gU2hvdWxkIGJlIGNhbGxlZCBldmVyeSB0aW1lIGJlZm9yZSByZXZhbGlkYXRpbmcgZm9ybVxuICogQHBhcmFtIHtlbGVtZW50fSBmb3JtIC0gRm9ybSB0byBiZSBjbGVhcmVkXG4gKiBAcmV0dXJucyB7dm9pZH1cbiAqL1xuZnVuY3Rpb24gY2xlYXJGb3JtRXJyb3JzKGZvcm0pIHtcbiAgICAkKGZvcm0pLmZpbmQoJy5mb3JtLWNvbnRyb2wuaXMtaW52YWxpZCcpLnJlbW92ZUNsYXNzKCdpcy1pbnZhbGlkJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZvcm1FbGVtZW50LCBwYXlsb2FkKSB7XG4gICAgLy8gY2xlYXIgZm9ybSB2YWxpZGF0aW9uIGZpcnN0XG4gICAgY2xlYXJGb3JtRXJyb3JzKGZvcm1FbGVtZW50KTtcbiAgICAkKCcuYWxlcnQnLCBmb3JtRWxlbWVudCkucmVtb3ZlKCk7XG5cbiAgICBpZiAodHlwZW9mIHBheWxvYWQgPT09ICdvYmplY3QnICYmIHBheWxvYWQuZmllbGRzKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKHBheWxvYWQuZmllbGRzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLmZpZWxkc1trZXldKSB7XG4gICAgICAgICAgICAgICAgdmFyIGZlZWRiYWNrRWxlbWVudCA9ICQoZm9ybUVsZW1lbnQpLmZpbmQoJ1tuYW1lPVwiJyArIGtleSArICdcIl0nKVxuICAgICAgICAgICAgICAgICAgICAucGFyZW50KClcbiAgICAgICAgICAgICAgICAgICAgLmNoaWxkcmVuKCcuaW52YWxpZC1mZWVkYmFjaycpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGZlZWRiYWNrRWxlbWVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHBheWxvYWRba2V5XSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZlZWRiYWNrRWxlbWVudC5odG1sKHBheWxvYWQuZmllbGRzW2tleV0uam9pbignPGJyLz4nKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuaHRtbChwYXlsb2FkLmZpZWxkc1trZXldKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBmZWVkYmFja0VsZW1lbnQuc2libGluZ3MoJy5mb3JtLWNvbnRyb2wnKS5hZGRDbGFzcygnaXMtaW52YWxpZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuICAgIGlmIChwYXlsb2FkICYmIHBheWxvYWQuZXJyb3IpIHtcbiAgICAgICAgdmFyIGZvcm0gPSAkKGZvcm1FbGVtZW50KS5wcm9wKCd0YWdOYW1lJykgPT09ICdGT1JNJ1xuICAgICAgICAgICAgPyAkKGZvcm1FbGVtZW50KVxuICAgICAgICAgICAgOiAkKGZvcm1FbGVtZW50KS5wYXJlbnRzKCdmb3JtJyk7XG5cbiAgICAgICAgZm9ybS5wcmVwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtZGFuZ2VyXCIgcm9sZT1cImFsZXJ0XCI+J1xuICAgICAgICAgICAgKyBwYXlsb2FkLmVycm9yLmpvaW4oJzxici8+JykgKyAnPC9kaXY+Jyk7XG4gICAgfVxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/login.js":
/*!**********************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/login.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar processInclude = __webpack_require__(/*! ./util */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js\");\n\n$(document).ready(function () {\n  processInclude(__webpack_require__(/*! ./login/login */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/login/login.js\"));\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvbG9naW4uanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvbG9naW4uanM/ZjIzZCJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBwcm9jZXNzSW5jbHVkZSA9IHJlcXVpcmUoJy4vdXRpbCcpO1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICAgcHJvY2Vzc0luY2x1ZGUocmVxdWlyZSgnLi9sb2dpbi9sb2dpbicpKTtcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/login.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/login/login.js":
/*!****************************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/login/login.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar formValidation = __webpack_require__(/*! ../components/formValidation */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/components/formValidation.js\");\n\nvar createErrorNotification = __webpack_require__(/*! ../components/errorNotification */ \"./cartridges/app_storefront_base_test/cartridge/client/default/js/components/errorNotification.js\");\n\nmodule.exports = {\n  login: function login() {\n    $('form.login').submit(function (e) {\n      var form = $(this);\n      e.preventDefault();\n      var url = form.attr('action');\n      form.spinner().start();\n      $('form.login').trigger('login:submit', e);\n      $.ajax({\n        url: url,\n        type: 'post',\n        dataType: 'json',\n        data: form.serialize(),\n        success: function success(data) {\n          form.spinner().stop();\n\n          if (!data.success) {\n            formValidation(form, data);\n            $('form.login').trigger('login:error', data);\n          } else {\n            $('form.login').trigger('login:success', data);\n            location.href = data.redirectUrl;\n          }\n        },\n        error: function error(data) {\n          if (data.responseJSON.redirectUrl) {\n            window.location.href = data.responseJSON.redirectUrl;\n          } else {\n            $('form.login').trigger('login:error', data);\n            form.spinner().stop();\n          }\n        }\n      });\n      return false;\n    });\n  },\n  register: function register() {\n    $('form.registration').submit(function (e) {\n      var form = $(this);\n      e.preventDefault();\n      var url = form.attr('action');\n      form.spinner().start();\n      $('form.registration').trigger('login:register', e);\n      console.log(form.serialize());\n      $.ajax({\n        url: url,\n        type: 'post',\n        dataType: 'json',\n        data: form.serialize(),\n        success: function success(data) {\n          form.spinner().stop();\n\n          if (!data.success) {\n            formValidation(form, data);\n          } else {\n            location.href = data.redirectUrl;\n          }\n        },\n        error: function error(err) {\n          if (err.responseJSON.redirectUrl) {\n            window.location.href = err.responseJSON.redirectUrl;\n          } else {\n            createErrorNotification($('.error-messaging'), err.responseJSON.errorMessage);\n          }\n\n          form.spinner().stop();\n        }\n      });\n      return false;\n    });\n  },\n  resetPassword: function resetPassword() {\n    $('.reset-password-form').submit(function (e) {\n      var form = $(this);\n      e.preventDefault();\n      var url = form.attr('action');\n      form.spinner().start();\n      $('.reset-password-form').trigger('login:register', e);\n      $.ajax({\n        url: url,\n        type: 'post',\n        dataType: 'json',\n        data: form.serialize(),\n        success: function success(data) {\n          form.spinner().stop();\n\n          if (!data.success) {\n            formValidation(form, data);\n          } else {\n            $('.request-password-title').text(data.receivedMsgHeading);\n            $('.request-password-body').empty().append('<p>' + data.receivedMsgBody + '</p>');\n\n            if (!data.mobile) {\n              $('#submitEmailButton').text(data.buttonText).attr('data-dismiss', 'modal');\n            } else {\n              $('.send-email-btn').empty().html('<a href=\"' + data.returnUrl + '\" class=\"btn btn-primary btn-block\">' + data.buttonText + '</a>');\n            }\n          }\n        },\n        error: function error() {\n          form.spinner().stop();\n        }\n      });\n      return false;\n    });\n  },\n  clearResetForm: function clearResetForm() {\n    $('#login .modal').on('hidden.bs.modal', function () {\n      $('#reset-password-email').val('');\n      $('.modal-dialog .form-control.is-invalid').removeClass('is-invalid');\n    });\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvbG9naW4vbG9naW4uanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvbG9naW4vbG9naW4uanM/ZWMwZCJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBmb3JtVmFsaWRhdGlvbiA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvZm9ybVZhbGlkYXRpb24nKTtcbnZhciBjcmVhdGVFcnJvck5vdGlmaWNhdGlvbiA9IHJlcXVpcmUoJy4uL2NvbXBvbmVudHMvZXJyb3JOb3RpZmljYXRpb24nKTtcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgbG9naW46IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnZm9ybS5sb2dpbicpLnN1Ym1pdChmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyIGZvcm0gPSAkKHRoaXMpO1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdmFyIHVybCA9IGZvcm0uYXR0cignYWN0aW9uJyk7XG4gICAgICAgICAgICBmb3JtLnNwaW5uZXIoKS5zdGFydCgpO1xuICAgICAgICAgICAgJCgnZm9ybS5sb2dpbicpLnRyaWdnZXIoJ2xvZ2luOnN1Ym1pdCcsIGUpO1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICB0eXBlOiAncG9zdCcsXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgICAgICBkYXRhOiBmb3JtLnNlcmlhbGl6ZSgpLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1WYWxpZGF0aW9uKGZvcm0sIGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJCgnZm9ybS5sb2dpbicpLnRyaWdnZXIoJ2xvZ2luOmVycm9yJywgZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCdmb3JtLmxvZ2luJykudHJpZ2dlcignbG9naW46c3VjY2VzcycsIGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYXRpb24uaHJlZiA9IGRhdGEucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5yZXNwb25zZUpTT04ucmVkaXJlY3RVcmwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gZGF0YS5yZXNwb25zZUpTT04ucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCdmb3JtLmxvZ2luJykudHJpZ2dlcignbG9naW46ZXJyb3InLCBkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9LFxuXG4gICAgcmVnaXN0ZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnZm9ybS5yZWdpc3RyYXRpb24nKS5zdWJtaXQoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIHZhciBmb3JtID0gJCh0aGlzKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBmb3JtLmF0dHIoJ2FjdGlvbicpO1xuICAgICAgICAgICAgZm9ybS5zcGlubmVyKCkuc3RhcnQoKTtcbiAgICAgICAgICAgICQoJ2Zvcm0ucmVnaXN0cmF0aW9uJykudHJpZ2dlcignbG9naW46cmVnaXN0ZXInLCBlKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGZvcm0uc2VyaWFsaXplKCkpO1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgICAgICAgICB0eXBlOiAncG9zdCcsXG4gICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgICAgICAgICAgICBkYXRhOiBmb3JtLnNlcmlhbGl6ZSgpLFxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1WYWxpZGF0aW9uKGZvcm0sIGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9jYXRpb24uaHJlZiA9IGRhdGEucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnIucmVzcG9uc2VKU09OLnJlZGlyZWN0VXJsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGVyci5yZXNwb25zZUpTT04ucmVkaXJlY3RVcmw7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVFcnJvck5vdGlmaWNhdGlvbigkKCcuZXJyb3ItbWVzc2FnaW5nJyksIGVyci5yZXNwb25zZUpTT04uZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIHJlc2V0UGFzc3dvcmQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnLnJlc2V0LXBhc3N3b3JkLWZvcm0nKS5zdWJtaXQoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIHZhciBmb3JtID0gJCh0aGlzKTtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHZhciB1cmwgPSBmb3JtLmF0dHIoJ2FjdGlvbicpO1xuICAgICAgICAgICAgZm9ybS5zcGlubmVyKCkuc3RhcnQoKTtcbiAgICAgICAgICAgICQoJy5yZXNldC1wYXNzd29yZC1mb3JtJykudHJpZ2dlcignbG9naW46cmVnaXN0ZXInLCBlKTtcbiAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgdHlwZTogJ3Bvc3QnLFxuICAgICAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgICAgICAgICAgICAgZGF0YTogZm9ybS5zZXJpYWxpemUoKSxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBmb3JtLnNwaW5uZXIoKS5zdG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtVmFsaWRhdGlvbihmb3JtLCBkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5yZXF1ZXN0LXBhc3N3b3JkLXRpdGxlJykudGV4dChkYXRhLnJlY2VpdmVkTXNnSGVhZGluZyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcucmVxdWVzdC1wYXNzd29yZC1ib2R5JykuZW1wdHkoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hcHBlbmQoJzxwPicgKyBkYXRhLnJlY2VpdmVkTXNnQm9keSArICc8L3A+Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEubW9iaWxlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1Ym1pdEVtYWlsQnV0dG9uJykudGV4dChkYXRhLmJ1dHRvblRleHQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdkYXRhLWRpc21pc3MnLCAnbW9kYWwnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnLnNlbmQtZW1haWwtYnRuJykuZW1wdHkoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuaHRtbCgnPGEgaHJlZj1cIidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsgZGF0YS5yZXR1cm5VcmxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsgJ1wiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1ibG9ja1wiPidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsgZGF0YS5idXR0b25UZXh0ICsgJzwvYT4nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGVycm9yOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc3Bpbm5lcigpLnN0b3AoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIGNsZWFyUmVzZXRGb3JtOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJyNsb2dpbiAubW9kYWwnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCgnI3Jlc2V0LXBhc3N3b3JkLWVtYWlsJykudmFsKCcnKTtcbiAgICAgICAgICAgICQoJy5tb2RhbC1kaWFsb2cgLmZvcm0tY29udHJvbC5pcy1pbnZhbGlkJykucmVtb3ZlQ2xhc3MoJ2lzLWludmFsaWQnKTtcbiAgICAgICAgfSk7XG4gICAgfVxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQXdCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJCQTtBQXVCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFySEEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/login/login.js\n");

/***/ }),

/***/ "./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js":
/*!*********************************************************************************!*\
  !*** ./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nmodule.exports = function (include) {\n  if (typeof include === 'function') {\n    include();\n  } else if (_typeof(include) === 'object') {\n    Object.keys(include).forEach(function (key) {\n      if (typeof include[key] === 'function') {\n        include[key]();\n      }\n    });\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jYXJ0cmlkZ2VzL2FwcF9zdG9yZWZyb250X2Jhc2VfdGVzdC9jYXJ0cmlkZ2UvY2xpZW50L2RlZmF1bHQvanMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NhcnRyaWRnZXMvYXBwX3N0b3JlZnJvbnRfYmFzZV90ZXN0L2NhcnRyaWRnZS9jbGllbnQvZGVmYXVsdC9qcy91dGlsLmpzP2U3NTUiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpbmNsdWRlKSB7XG4gICAgaWYgKHR5cGVvZiBpbmNsdWRlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGluY2x1ZGUoKTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBpbmNsdWRlID09PSAnb2JqZWN0Jykge1xuICAgICAgICBPYmplY3Qua2V5cyhpbmNsdWRlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgaW5jbHVkZVtrZXldID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgaW5jbHVkZVtrZXldKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn07XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./cartridges/app_storefront_base_test/cartridge/client/default/js/util.js\n");

/***/ })

/******/ });